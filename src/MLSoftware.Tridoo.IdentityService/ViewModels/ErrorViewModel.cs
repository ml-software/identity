﻿using IdentityServer4.Models;

namespace MLSoftware.Tridoo.IdentityService.ViewModels
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}
