﻿using System.ComponentModel.DataAnnotations;

namespace MLSoftware.Tridoo.IdentityService.ViewModels
{
    public class LoginInputModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberLogin { get; set; }

        public string ReturnUrl { get; set; }
    }
}
