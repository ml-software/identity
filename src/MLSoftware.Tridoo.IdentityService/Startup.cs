﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using MLSoftware.Tridoo.IdentityService.Data;
using MLSoftware.Tridoo.IdentityService.Services;
using MLSoftware.Core.Security;
using MLSoftware.Core;
using System.Reflection;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Rewrite;
using Serilog;
using MLSoftware.Core.Utilities.Logging;

namespace MLSoftware.Tridoo.IdentityService
{
    public class Startup : MicroServiceStartupBase
    {
        public Startup(IHostingEnvironment env) : base(env)
        {
        }

        public override void ConfigureBuilder(IConfigurationBuilder builder)
        {
            if(_env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            base.ConfigureBuilder(builder);
        }

        protected override void ConfigureLogging(IHostingEnvironment env)
        {
            Log.Logger = LogConfiguration.ConfigureLogging(env).WriteTo.RollingFile("Logs/log-{Date}.txt").CreateLogger();
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            base.ConfigureDependencyInjection(services);

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            // Add framework services.
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(builder =>
            {
                builder.UseSqlite(connectionString, options => options.MigrationsAssembly(migrationsAssembly).MigrationsHistoryTable("MigrationHistory"));
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            })
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            services.Configure<AppSettings>(Configuration);            

            if (_env.IsDevelopment())
            {
                services.Configure<MailGunOptions>(options => 
                {
                    options.MailGunDomainName = "sandboxef399b1225e84a3197d14fa01964685b.mailgun.org";
                    options.MailGunApiKey = "key-ec9dcb7b97f21c3f7ca8efb241c1078e";
                });
            }
            else
            {
                services.Configure<MailGunOptions>(Configuration);
            }

            services.AddTransient<IEmailSender, MailGunMessageSender>();
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();

            // Adds IdentityServer
            services.AddIdentityServer(x => x.IssuerUri = Configuration.GetValue<string>("IdentityUrl"))
                .AddTemporarySigningCredential()
                .AddConfigurationStore(builder =>
                    builder.UseSqlite(connectionString, options => options.MigrationsAssembly(migrationsAssembly).MigrationsHistoryTable("MigrationHistory")))
                .AddOperationalStore(builder =>
                    builder.UseSqlite(connectionString, options => options.MigrationsAssembly(migrationsAssembly).MigrationsHistoryTable("MigrationHistory")))
                .AddAspNetIdentity<ApplicationUser>()
                .AddExtensionGrantValidator<DelegationGrantValidator>()
                .Services.AddTransient<IProfileService, ProfileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            // Add Serilog to the logging pipeline
            loggerFactory.AddSerilog();

            app.UseRewriter(new RewriteOptions()
                .AddIISUrlRewrite(env.ContentRootFileProvider, "urlRewrite.config"));

            // Rewrite forwarded headers due to proxy
            //app.UseForwardedHeaders(new ForwardedHeadersOptions
            //{
            //    ForwardedHeaders = ForwardedHeaders.XForwardedProto
            //});

            // static file
            app.UseStaticFiles();

            app.UseIdentity();

            // Adds IdentityServer
            app.UseIdentityServer();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //Seed Data
            new ApplicationContextSeed(new PasswordHasher<ApplicationUser>()).SeedAsync(app, loggerFactory, Configuration).Wait();
        }
    }
}
