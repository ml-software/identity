﻿using System.Threading.Tasks;

namespace MLSoftware.Tridoo.IdentityService.Services
{
    public interface ILoginService<T>
    {
        Task<bool> ValidateCredentials(T user, string password);
        Task<T> FindByUsername(string user);
        Task SignIn(T user);
    }
}
