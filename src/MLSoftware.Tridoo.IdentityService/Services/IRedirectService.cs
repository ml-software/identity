﻿namespace MLSoftware.Tridoo.IdentityService.Services
{
    public interface IRedirectService
    {
        string ExtractRedirectUriFromReturnUrl(string url);
    }
}
