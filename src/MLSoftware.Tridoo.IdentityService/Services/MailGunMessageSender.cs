﻿using System;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.Mailgun;
using Microsoft.Extensions.Options;
using MLSoftware.Core;

namespace MLSoftware.Tridoo.IdentityService.Services
{
    public class MailGunMessageSender : IEmailSender
    {
        private readonly MailGunOptions _mailGunOptions;

        public MailGunMessageSender(IOptions<MailGunOptions> optionsAccessor)
        {
            _mailGunOptions = optionsAccessor.Value;

            if (string.IsNullOrEmpty(_mailGunOptions.MailGunDomainName))
                throw new ArgumentNullException(nameof(_mailGunOptions.MailGunDomainName));

            if (string.IsNullOrEmpty(_mailGunOptions.MailGunApiKey))
                throw new ArgumentNullException(nameof(_mailGunOptions.MailGunApiKey));
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = Email
                    .From("info@ml-software.ch", "Tridoo")
                    .To(email)
                    .Subject(subject)
                    .BodyAsHtml()
                    .Body(message);

            emailMessage.Sender = new MailgunSender(_mailGunOptions.MailGunDomainName, _mailGunOptions.MailGunApiKey);

            var response = await emailMessage.SendAsync();
            if (!response.Successful)
            {
                throw new Exception(string.Join(Environment.NewLine, response.ErrorMessages));
            }
        }
    }
}
