﻿using IdentityServer4.Models;
using System.Collections.Generic;
using static IdentityServer4.IdentityServerConstants;

namespace MLSoftware.Tridoo.IdentityService
{
    public class Config
    {
        // scopes define the resources in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                //Authentication OpenId uses this scopes;
                new ApiResource(StandardScopes.OpenId),
                new ApiResource(StandardScopes.Profile),

                //Each api we want to securice;
                new ApiResource("guests", "Guest Service"),
                new ApiResource("hotels", "Hotel Service"),
                new ApiResource("rooms", "Room Service"),
                new ApiResource("bookings", "Booking Service")
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "web",
                    ClientName = "Web client",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    ClientUri = $"{clientsUrl["web"]}",
                    AllowedGrantTypes = GrantTypes.Hybrid,
                    RequireConsent = false,
                    RedirectUris = new List<string>
                    {
                        $"{clientsUrl["web"]}/signin-oidc",
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"{clientsUrl["web"]}/signout-callback-oidc"
                    },
                    RefreshTokenUsage = TokenUsage.ReUse,
                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.OfflineAccess,
                        "guests",
                        "hotels",
                        "rooms",
                        "bookings"
                    },
                    AllowOfflineAccess = true,
                    //UpdateAccessTokenClaimsOnRefresh = true,
                    AccessTokenLifetime = 1
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }
    }
}
