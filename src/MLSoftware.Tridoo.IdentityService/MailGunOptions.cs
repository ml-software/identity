﻿namespace MLSoftware.Tridoo.IdentityService
{
    public class MailGunOptions
    {
        public string MailGunDomainName { get; set; }
        public string MailGunApiKey { get; set; }
    }
}