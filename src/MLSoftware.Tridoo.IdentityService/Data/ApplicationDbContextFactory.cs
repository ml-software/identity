﻿using System;
using System.Reflection;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace MLSoftware.Tridoo.IdentityService.Data
{
    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(DbContextFactoryOptions options)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlite("Data Source=.\\identity.db", mo => mo.MigrationsAssembly(migrationsAssembly));
            return new ApplicationDbContext(builder.Options);
        }
    }

    public class ConfigurationDbContextFactory : IDbContextFactory<ConfigurationDbContext>
    {
        public ConfigurationDbContext Create(DbContextFactoryOptions options)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var builder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            builder.UseSqlite("Data Source=.\\identity.db", mo => mo.MigrationsAssembly(migrationsAssembly));
            return new ConfigurationDbContext(builder.Options, new ConfigurationStoreOptions());
        }
    }

    public class PersistedGrantDbContextFactory : IDbContextFactory<PersistedGrantDbContext>
    {
        public PersistedGrantDbContext Create(DbContextFactoryOptions options)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var builder = new DbContextOptionsBuilder<PersistedGrantDbContext>();
            builder.UseSqlite("Data Source=.\\identity.db", mo => mo.MigrationsAssembly(migrationsAssembly));
            return new PersistedGrantDbContext(builder.Options, new OperationalStoreOptions());
        }
    }
}
