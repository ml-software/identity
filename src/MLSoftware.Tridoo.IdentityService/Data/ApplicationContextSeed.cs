﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MLSoftware.Core;

namespace MLSoftware.Tridoo.IdentityService.Data
{
    public class ApplicationContextSeed
    {
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;

        public ApplicationContextSeed(IPasswordHasher<ApplicationUser> passwordHasher)
        {
            _passwordHasher = passwordHasher;
        }

        public async Task SeedAsync(IApplicationBuilder app, ILoggerFactory loggerFactory, IConfigurationRoot configuration, int? retry = 0)
        {
            var retryForAvaiability = retry.Value;

            try
            {
                using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
                {
                    var applicationDbContext = (ApplicationDbContext)app.ApplicationServices.GetService(typeof(ApplicationDbContext));

                    applicationDbContext.Database.Migrate();

                    if (!applicationDbContext.Users.Any())
                    {
                        applicationDbContext.Users.Add(GetDefaultUser());

                        await applicationDbContext.SaveChangesAsync();
                    }

                    serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                    var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                    context.Database.Migrate();

                    if (!context.Clients.Any())
                    {
                        var clientUrls = new Dictionary<string, string>
                        {
                            { "web", configuration.GetValue<string>("WebClient") }
                        };

                        foreach (var client in Config.GetClients(clientUrls))
                        {
                            context.Clients.Add(client.ToEntity());
                        }
                        await context.SaveChangesAsync();
                    }

                    if (!context.IdentityResources.Any())
                    {
                        foreach (var resource in Config.GetIdentityResources())
                        {
                            context.IdentityResources.Add(resource.ToEntity());
                        }
                        context.SaveChanges();
                    }

                    if (!context.ApiResources.Any())
                    {
                        foreach (var resource in Config.GetApiResources())
                        {
                            context.ApiResources.Add(resource.ToEntity());
                        }
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 30)
                {
                    retryForAvaiability++;
                    var log = loggerFactory.CreateLogger("catalog seed");
                    log.LogError(ex.Message);
                    await Task.Delay(1000);
                    await SeedAsync(app, loggerFactory, configuration, retryForAvaiability);
                }
            }
        }

        private ApplicationUser GetDefaultUser()
        {
            var user =
            new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                City = "Basel",
                Country = "Switzerland",
                Email = "demouser@ml-software.ch",
                LastName = "DemoLastName",
                Name = "DemoUser",
                PhoneNumber = "1234567890",
                UserName = "demouser@ml-software.ch",
                Zip = "4055",
                Street = "Bahnhofstrasse 4",
                NormalizedEmail = "DEMOUSER@ML-SOFTWARE.CH",
                NormalizedUserName = "DEMOUSER@ML-SOFTWARE.CH",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                EmailConfirmed = true,
                HotelId = 1
            };

            user.PasswordHash = _passwordHasher.HashPassword(user, "Pass@word1");

            return user;
        }
    }
}
